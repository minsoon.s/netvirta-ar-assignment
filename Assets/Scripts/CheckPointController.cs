﻿namespace netvirtaAR
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using GoogleARCore;
    using UnityEngine.UI;

    public class CheckPointController : MonoBehaviour
    {
        public GameObject checkPointPrefab;
        public float distanceToARObj = 1f;
        public float spawnHeight = 1f;
        public InputField checkPointInputField;
        public Text snackBarText;
        public GameObject snackBarUI;
        public GameObject okButtonObject;
        public GameObject checkPointMenu;
        public UserGuidanceController userGuidanceCtrl;
        public GameObject scanningGraphic;

        private Anchor myARObjAnchor;
        private Pose myARObjPose;
        private DetectedPlane detectedPlane;
        private int numOfCheckPoints;
        private List<GameObject> checkPointsList = new List<GameObject>();

        // number of rows(longitude) that sub divide the sphere for checkpoints
        private const int nRows = 5;
        // angle between each row from the center of the sphere (pitch)
        private const float pAngle = 20f;
        // displacement in angle between the rows (yaw), calculated using angleBetween
        private float yAngle;

        void Start()
        {
            checkPointInputField.onEndEdit.AddListener(ProcessCheckPoints);

            okButtonObject.GetComponent<Button>().onClick.AddListener(OkButtonClicked);
            okButtonObject.SetActive(false);
            checkPointMenu.SetActive(false);
            scanningGraphic.SetActive(false);
        }

        void Update()
        {

        }

        private void SpawnCheckPoints()
        {
            int checkpointsPerRow = numOfCheckPoints / nRows;
            float angleBetween = 360f / (float)checkpointsPerRow;
            yAngle = angleBetween / 2f;

            // start at the top most row, so we angle the vector upwards
            float pitch = pAngle * ((nRows - 1) / 2);

            // yaw for displacing checkpoints on each row
            float yaw = 0f;

            for (int j = 0; j < nRows; ++j)
            {
                Vector3 vecToCheckPoint = myARObjPose.forward * distanceToARObj;
                vecToCheckPoint = Quaternion.AngleAxis(pitch, myARObjPose.right) * vecToCheckPoint;
                vecToCheckPoint = Quaternion.AngleAxis(yaw, myARObjPose.up) * vecToCheckPoint;

                // generate checkpoints for each longitudal row
                for (int i = 0; i < checkpointsPerRow; ++i)
                {
                    // get spawn position for checkpoint
                    Vector3 spawnPos = myARObjPose.position + vecToCheckPoint;
                    spawnPos.y += spawnHeight;

                    Vector3 objCenter = myARObjPose.position;
                    objCenter.y += spawnHeight;
                    Quaternion checkPointRot = Quaternion.LookRotation(objCenter - spawnPos);

                    // instantiate the checkpoint
                    GameObject checkPointInstance = Instantiate(checkPointPrefab, spawnPos,
                                                                checkPointRot, myARObjAnchor.transform);
                    checkPointInstance.tag = "checkpoint";

                    // face the particle system towards the AR object
                    //Vector3 targetARObjPos = myARObjPose.position;
                    //targetARObjPos.y += spawnHeight;
                    // create a fixed rotation for the particle system
                    Quaternion fixedRotation = Quaternion.LookRotation(objCenter - checkPointInstance.transform.position);
                    checkPointInstance.GetComponent<CheckPoint>().SetParticleRotation(fixedRotation);

                    checkPointsList.Add(checkPointInstance);

                    // rotate the spawn vector to the next spawn angle
                    vecToCheckPoint = Quaternion.AngleAxis(angleBetween, myARObjPose.up) * vecToCheckPoint;
                }

                // lower pitch to the next row
                pitch -= pAngle;
                // adjust yaw to displace checkpoints for next row
                yaw += yAngle;
            }
        }

        private void ProcessCheckPoints(string text)
        {
            int num;
            if(int.TryParse(text, out num))
            {
                // 50 fixed as the highest amount of checkpoints acceptable
                if (num > 50)
                {
                    snackBarText.text = "Please enter a number lower than 50!";
                }
                else if (num <= 0)
                {
                    snackBarText.text = "Please enter a number higher than 0!";
                }
                else
                {
                    numOfCheckPoints = num;
                    okButtonObject.SetActive(true);
                    snackBarText.text = "Generate " + numOfCheckPoints + " checkpoints?";
                }
            }
            else
            {
                // something bad happened with the input!
                snackBarText.text = "Please enter a number between 1 to 50";
            }
        }

        private void OkButtonClicked()
        {
            SpawnCheckPoints();


            okButtonObject.SetActive(false);
            checkPointMenu.SetActive(false);
            //snackBarText.text = numOfCheckPoints + " checkpoints generated";
            snackBarUI.SetActive(false);

            scanningGraphic.SetActive(true);
        }

        public void SetAnchor(Anchor anchor)
        {
            myARObjAnchor = anchor;
            myARObjPose = new Pose(anchor.transform.position, anchor.transform.rotation);

            checkPointMenu.SetActive(true);
        }

        public void SetPlane(DetectedPlane plane)
        {
            detectedPlane = plane;
        }

        public void OnStartButtonTouched()
        {
            userGuidanceCtrl.SetCheckpointsList(checkPointsList);
            userGuidanceCtrl.viewfinder.enabled = true;
        }
    }
}