﻿namespace netvirtaAR
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class ViewfinderController : MonoBehaviour
    {
        public RawImage viewfinderImage;
        public float blinkInterval;

        private bool isBlinking;
        private float maxBlinkDuration;
        private float blinkTime;

        // Use this for initialization
        void Start()
        {
            blinkTime = 0f;
            isBlinking = false;
            viewfinderImage.enabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if(isBlinking)
            {
                blinkTime += Time.deltaTime;

                if(blinkTime >= maxBlinkDuration)
                {
                    // over duration, stop blinking
                    StopBlinking();
                }
            }
        }

        public void StartBlinking()
        {
            if(isBlinking)
            {
                return;
            }

            if(viewfinderImage != null)
            {
                isBlinking = true;
                blinkTime = 0f;
                InvokeRepeating("ToggleViewfinderBlink", 0f, blinkInterval);
            }
        }

        public void StopBlinking()
        {
            CancelInvoke();
            isBlinking = false;
            blinkTime = 0f;
        }

        private void ToggleViewfinderBlink()
        {
            viewfinderImage.enabled = !viewfinderImage.enabled;
        }

        public void ShowViewfinder(bool show)
        {
            viewfinderImage.enabled = show;
        }

        public void MaxBlinkDuration(float t)
        {
            maxBlinkDuration = t;
        }

        public bool IsViewfinderShowing()
        {
            return viewfinderImage.enabled;
        }
    }
}