﻿namespace netvirtaAR
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;
    using DG.Tweening;

    public class ArrowGuideFade : MonoBehaviour
    {
        public RawImage arrowImage;
        //private Color fadedColor;
        //private Color arrowColor;
        //private bool isFading;
        //private float t;
        //private float fadeDuration = 2f;
        //private CanvasRenderer rend;

        public float blinkInterval = 1f;
        private bool isBlinking;

        // Use this for initialization
        void Start()
        {
            //arrowImage = GetComponent<RawImage>();
            //arrowColor = arrowImage.color;
            //Color newColor = arrowImage.color;
            //newColor.a = 0f;
            //fadedColor = newColor;
            
            //isFading = true;
            //t = 0f;

            //rend = GetComponent<CanvasRenderer>();

            StartBlinking();
        }

        // Update is called once per frame
        void Update()
        {
            //if(isFading)
            //{
            //    arrowImage.CrossFadeAlpha(0f, fadeDuration, false);
            //}
            //else
            //{
            //    arrowImage.CrossFadeAlpha(1f, fadeDuration, false);
            //}
            ////arrowImage.color = Color.Lerp(arrowColor, fadedColor, Mathf.PingPong(Time.time, 1f));
            ////arrowImage.color = Color.Lerp(Color.white, Color.magenta, t);

            ////arrowImage.DOFade(0f, 1f);
            ////arrowImage.color = Color.red;

            //t += Time.deltaTime;
            //if(t > fadeDuration)
            //{
            //    isFading = !isFading;
            //    t = 0f;
            //}
        }

        private void StartBlinking()
        {
            if(isBlinking)
            {
                return;
            }

            if(arrowImage != null)
            {
                isBlinking = true;
                InvokeRepeating("ToggleArrow", 0f, blinkInterval);
            }
        }

        private void ToggleArrow()
        {
            arrowImage.enabled = !arrowImage.enabled;
        }
    }
}