﻿namespace netvirtaAR
{
    using System.Collections.Generic;
    using GoogleARCore;
    using UnityEngine;
    using UnityEngine.UI;
    using GoogleARCore.Examples.Common;

#if UNITY_EDITOR
    // Set up touch input propagation while using Instant Preview in the editor.
    using Input = GoogleARCore.InstantPreviewInput;
#endif

    public class ARController : MonoBehaviour
    {
        public Camera firstPersonCamera;
        public GameObject ARObjectPrefab;
        public GameObject snackbarUI;
        public Text snackbarText;
        public CheckPointController checkPointController;
        public DetectedPlaneGenerator detectedPlaneGenerator;

        /// The rotation in degrees need to apply to model when the Andy model is placed.
        private const float k_ModelRotation = 180.0f;
        /// A list to hold all planes ARCore is tracking in the current frame. This object is used across
        /// the application to avoid per-frame allocations.
        private List<DetectedPlane> m_AllPlanes = new List<DetectedPlane>();
        private bool m_IsQuitting = false;

        private GameObject myARObject;
        private Anchor myARAnchor;

        public void Start()
        {
            QuitOnConnectionErrors();

            snackbarText.text = "Searching for surfaces...";
        }

        public void Update()
        {
            UpdateApplicationLifecycle();

            // Check if a plane is found and change display text accordingly
            if(CheckPlaneTrackingStatus() && myARObject == null)
            {
                snackbarText.text = "Tap to place object";
            }

            //snackbarUI.SetActive(showSearchingUI);

            ProcessTouches();
        }

        private void ProcessTouches()
        {
            // If the player has not touched the screen, we are done with this update.
            Touch touch;
            if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Began)
            {
                return;
            }

            // Raycast against the location the player touched to search for planes.
            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
                TrackableHitFlags.FeaturePointWithSurfaceNormal;

            if (Frame.Raycast(touch.position.x, touch.position.y, raycastFilter, out hit))
            {
                // Use hit pose and camera pose to check if hittest is from the
                // back of the plane, if it is, no need to create the anchor.
                if ((myARObject == null) &&
                    (hit.Trackable is DetectedPlane) &&
                    Vector3.Dot(firstPersonCamera.transform.position - hit.Pose.position,
                        hit.Pose.rotation * Vector3.up) > 0)
                {
                    CreateARObject(hit);
                }
            }
        }

        private void CreateARObject(TrackableHit hit)
        {
            // Instantiate Andy model at the hit pose.
            myARObject = Instantiate(ARObjectPrefab, hit.Pose.position, hit.Pose.rotation);

            // Compensate for the hitPose rotation facing away from the raycast (i.e. camera).
            myARObject.transform.Rotate(0, k_ModelRotation, 0, Space.Self);

            // Create an anchor to allow ARCore to track the hitpoint as understanding of the physical
            // world evolves.
            myARAnchor = hit.Trackable.CreateAnchor(hit.Pose);

            // Make Andy model a child of the anchor.
            myARObject.transform.parent = myARAnchor.transform;

            snackbarText.text = "Object placed!";

            checkPointController.SetAnchor(myARAnchor);

            detectedPlaneGenerator.enabled = false;
        }

        private bool CheckPlaneTrackingStatus()
        {
            Session.GetTrackables<DetectedPlane>(m_AllPlanes);
            bool planeFound = false;
            for (int i = 0; i < m_AllPlanes.Count; i++)
            {
                if (m_AllPlanes[i].TrackingState == TrackingState.Tracking)
                {
                    planeFound = true;
                    break;
                }
            }

            return planeFound;
        }

        private void UpdateApplicationLifecycle()
        {
            // Exit the app when the 'back' button is pressed.
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }

            // Only allow the screen to sleep when not tracking.
            if (Session.Status != SessionStatus.Tracking)
            {
                const int lostTrackingSleepTimeout = 15;
                Screen.sleepTimeout = lostTrackingSleepTimeout;
            }
            else
            {
                Screen.sleepTimeout = SleepTimeout.NeverSleep;
            }

            if (m_IsQuitting)
            {
                return;
            }

            QuitOnConnectionErrors();
        }

        private void QuitOnConnectionErrors()
        {
            // Quit if ARCore was unable to connect and give Unity some time for the toast to appear.
            if (Session.Status == SessionStatus.ErrorPermissionNotGranted)
            {
                _ShowAndroidToastMessage("Camera permission is needed to run this application.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
            else if (Session.Status.IsError())
            {
                _ShowAndroidToastMessage("ARCore encountered a problem connecting.  Please start the app again.");
                m_IsQuitting = true;
                Invoke("_DoQuit", 0.5f);
            }
        }

        private void _DoQuit()
        {
            Application.Quit();
        }

        /// <summary>
        /// Show an Android toast message.
        /// </summary>
        /// <param name="message">Message string to show in the toast.</param>
        private void _ShowAndroidToastMessage(string message)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject unityActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            if (unityActivity != null)
            {
                AndroidJavaClass toastClass = new AndroidJavaClass("android.widget.Toast");
                unityActivity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                {
                    AndroidJavaObject toastObject = toastClass.CallStatic<AndroidJavaObject>("makeText", unityActivity,
                        message, 0);
                    toastObject.Call("show");
                }));
            }
        }
    }
}
