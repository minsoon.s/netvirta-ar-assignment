﻿namespace netvirtaAR
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class DrawGLLines : MonoBehaviour
    {
        public Material lineMaterial;

        private bool startDrawing = false;
        private List<Vector3> vertexPosList = new List<Vector3>();

        private void DrawConnectingLines()
        {
            if(vertexPosList.Count <= 1)
            {
                return;
            }

            for (int i = 0; i < vertexPosList.Count - 1; ++i)
            {
                DrawLine(vertexPosList[i], vertexPosList[i + 1]);
            }
        }

        private void DrawLine(Vector3 startPos, Vector3 endPos)
        {
            GL.Begin(GL.LINES);
            lineMaterial.SetPass(0);
            GL.Color(new Color(lineMaterial.color.r, lineMaterial.color.g, lineMaterial.color.b, lineMaterial.color.a));
            GL.Vertex3(startPos.x, startPos.y, startPos.z);
            GL.Vertex3(endPos.x, endPos.y, endPos.z);
            GL.End();
        }

        private void OnPostRender()
        {
            if(startDrawing)
            {
                DrawConnectingLines();
            }
        }

        public void StartDrawingLines(List<Vector3> checkpointList)
        {
            startDrawing = true;
            vertexPosList.AddRange(checkpointList);
        }

        public void StartDrawingLine(Vector3 start, Vector3 end)
        {
            startDrawing = true;
            vertexPosList.Add(start);
            vertexPosList.Add(end);
        }
    }
}