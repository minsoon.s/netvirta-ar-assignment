﻿namespace netvirtaAR
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class UserGuidanceController : MonoBehaviour
    {
        public Camera firstPersonCamera;
        public RawImage viewfinder;
        // unity cant serialize dictionary so use list as staging
        public List<GameObject> arrowGuideList;
        public GameObject scanningHelpObj;
        public float userIdleDuration = 5f;
        public GameObject scanCompleteObj;

        private List<GameObject> checkpointsList;
        private GameObject selectedCheckPoint;
        private Dictionary<string, GameObject> arrowGuideDictionary;
        //private List<GameObject> scannedCheckPointList;
        private float userIdleTime;
        private bool helpShown;

        void Start()
        {
            // put the arrow objects into a dictionary for easy access later
            arrowGuideDictionary = new Dictionary<string, GameObject>();
            foreach(GameObject arrowGuide in arrowGuideList)
            {
                arrowGuide.SetActive(false);
                arrowGuideDictionary.Add(arrowGuide.name, arrowGuide);
            }

            //scannedCheckPointList = new List<GameObject>();
            userIdleTime = 0f;
            helpShown = false;
            scanningHelpObj.SetActive(false);
            scanCompleteObj.SetActive(false);
        }

        void Update()
        {
            if (checkpointsList == null)
            {
                return;
            }

            // update the checkpoint list by culling scanned points
            //checkpointsList.RemoveAll(checkpoint => checkpoint.GetComponent<CheckPoint>().IsScanned());
            if(selectedCheckPoint && selectedCheckPoint.GetComponent<CheckPoint>().IsScanned())
            {
                checkpointsList.Remove(selectedCheckPoint);
                selectedCheckPoint = null;
            }

            // stop guidance while user is scanning or when help is showing
            if(selectedCheckPoint && 
               selectedCheckPoint.GetComponent<CheckPoint>().IsScanningNow() ||
               scanningHelpObj.activeSelf)
            {
                return;
            }

            // display arrow guide when checkpoint is outside viewport
            if (FindNearestCheckPoint() && !selectedCheckPoint.GetComponent<CheckPoint>().IsVisibleInViewport())
            {
                Vector3 checkPointVector = selectedCheckPoint.transform.position - firstPersonCamera.transform.position;

                float leftRightDotProduct = Vector3.Dot(checkPointVector, firstPersonCamera.transform.right);
                float upDownDotProduct = Vector3.Dot(checkPointVector, firstPersonCamera.transform.up);

                // check which direction has higher weightage
                if(Mathf.Abs(leftRightDotProduct) > Mathf.Abs(upDownDotProduct))
                {
                    // left or right
                    if(leftRightDotProduct < 0f)
                    {
                        ShowArrowGuide("ArrowGuide_Left");
                    }
                    else
                    {
                        ShowArrowGuide("ArrowGuide_Right");
                    }
                }
                else
                {
                    // up or down
                    if(upDownDotProduct < 0f)
                    {
                        ShowArrowGuide("ArrowGuide_Down");
                    }
                    else
                    {
                        ShowArrowGuide("ArrowGuide_Up");
                    }
                }

                // reset timer if user is not looking at the checkpoint
                userIdleTime = 0f;
            }
            else
            {
                HideAllArrows();

                if (!helpShown)
                {
                    userIdleTime += Time.deltaTime;
                    if (userIdleTime > userIdleDuration)
                    {
                        scanningHelpObj.SetActive(true);

                        // we only want to show help once
                        helpShown = true;
                    }
                }
            }
        }

        private bool FindNearestCheckPoint()
        {
            // when there are no more checkpoints, we are done
            if(checkpointsList.Count == 0)
            {
                scanCompleteObj.SetActive(true);
                viewfinder.enabled = false;

                return false;
            }

            float distance = float.MaxValue;
            GameObject newCheckPoint = null;

            foreach (GameObject checkpoint in checkpointsList)
            {
                // find the checkpoint nearest to the camera
                float sqDist = (firstPersonCamera.transform.position - checkpoint.transform.position).sqrMagnitude;
                if(sqDist < distance)
                {
                    distance = sqDist;
                    newCheckPoint = checkpoint;
                }
            }

            // deselect the current checkpoint first
            if(selectedCheckPoint != null)
            {
                selectedCheckPoint.GetComponent<CheckPoint>().DeselectCheckPoint();
            }

            selectedCheckPoint = newCheckPoint;
            selectedCheckPoint.GetComponent<CheckPoint>().SelectCheckPoint();

            return true;
        }

        private void HideAllArrows()
        {
            foreach(KeyValuePair<string, GameObject> arrowGuidePair in arrowGuideDictionary)
            {
                arrowGuidePair.Value.SetActive(false);
            }
        }

        private void ShowArrowGuide(string arrowName)
        {
            HideAllArrows();

            GameObject arrowGuide = arrowGuideDictionary[arrowName];
            arrowGuide.SetActive(true);
        }

        public void SetCheckpointsList(List<GameObject> cpList)
        {
            checkpointsList = cpList;
        }
    }
}