﻿namespace netvirtaAR
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.UI;

    public class ScanningController : MonoBehaviour
    {
        public Camera firstPersonCamera;
        public ViewfinderController viewfinder;
        public float scanDuration = 4f;
        public GameObject checkmarkObj;
        public float checkmarkDuration = 2f;
        public AudioSource scanCompleteAudio;
        public GameObject errorMessageBoxObj;
        public Image loadingBar;

        private bool isScanning;
        private float scanTime;
        private GameObject scanObject;
        private float checkmarkShowTime;
        private GameObject loadingBarObj;

        private void Start()
        {
            isScanning = false;
            scanTime = 0f;

            viewfinder.MaxBlinkDuration(scanDuration);

            checkmarkShowTime = 0f;
            checkmarkObj.SetActive(false);

            errorMessageBoxObj.SetActive(false);

            loadingBar.fillAmount = 0f;
            // get the parent object that holds the frame and the bar
            loadingBarObj = loadingBar.gameObject.transform.parent.gameObject;
            loadingBarObj.SetActive(false);
        }

        private void Update()
        {
            if(isScanning)
            {
                // raycast to check if camera is looking at the AR object
                int layerMask = 1 << LayerMask.NameToLayer("ARObject");
                RaycastHit hit;
                if (Physics.Raycast(firstPersonCamera.transform.position,
                                   firstPersonCamera.transform.forward,
                                   out hit,
                                   Mathf.Infinity,
                                   layerMask))
                {
                    // scanning in progress
                    scanTime += Time.deltaTime;

                    viewfinder.StartBlinking();
                    scanObject.GetComponent<CheckPoint>().ScanningCheckPoint(true);

                    errorMessageBoxObj.SetActive(false);

                    loadingBarObj.SetActive(true);
                    loadingBar.fillAmount = scanTime / scanDuration;
                }
                else
                {
                    // camera is not looking at the AR object, we reset the timer
                    scanTime = 0f;

                    viewfinder.StopBlinking();
                    viewfinder.ShowViewfinder(true);
                    scanObject.GetComponent<CheckPoint>().ScanningCheckPoint(false);

                    errorMessageBoxObj.SetActive(true);

                    loadingBarObj.SetActive(false);
                    loadingBar.fillAmount = 0f;
                }

                if(scanTime >= scanDuration)
                {
                    // done scanning
                    scanObject.GetComponent<CheckPoint>().CompleteScan();
                    scanObject = null;

                    scanTime = 0f;
                    viewfinder.StopBlinking();

                    checkmarkObj.SetActive(true);
                    viewfinder.ShowViewfinder(false);
                    loadingBarObj.SetActive(false);

                    scanCompleteAudio.Play();
                }

                // reset bool so on next frame, check collision again for scanning status
                isScanning = false;
            }

            if(checkmarkObj.activeSelf)
            {
                checkmarkShowTime += Time.deltaTime;

                if(checkmarkShowTime > checkmarkDuration)
                {
                    checkmarkShowTime = 0f;
                    checkmarkObj.SetActive(false);
                    viewfinder.ShowViewfinder(true);
                }
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if(other.gameObject.tag == "checkpoint")
            {
                if(other.gameObject.GetComponent<CheckPoint>().IsScanned())
                {
                    return;
                }

                isScanning = true;
                scanObject = other.gameObject;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "checkpoint")
            {
                if (other.gameObject.GetComponent<CheckPoint>().IsScanned())
                {
                    return;
                }

                // start scanning process on entering trigger
                scanTime = 0f;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.tag == "checkpoint")
            {
                if (other.gameObject.GetComponent<CheckPoint>().IsScanned())
                {
                    return;
                }

                errorMessageBoxObj.SetActive(false);
                loadingBarObj.SetActive(false);

                viewfinder.StopBlinking();
                viewfinder.ShowViewfinder(true);
                scanObject.GetComponent<CheckPoint>().ScanningCheckPoint(false);
            }
        }
    }
}