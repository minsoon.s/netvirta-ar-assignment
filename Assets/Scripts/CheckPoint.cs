﻿namespace netvirtaAR
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    public class CheckPoint : MonoBehaviour
    {
        public GameObject checkPointModel;
        public ParticleSystem rayEffect;
        public Color activeColor;
        public Color inactiveColor;

        private bool isSelected;
        private Renderer rend;
        private Quaternion particleFixedRotation;
        private GameObject particleObj;
        //private readonly Vector3 rotAxis = new Vector3(1f, 1f, -1f);
        private bool isScanned;
        private bool scanningInProgress;

        void Start()
        {
            particleObj = rayEffect.gameObject;

            rend = checkPointModel.GetComponent<Renderer>();
            if(rend != null)
            {
                isSelected = false;
                isScanned = false;
                scanningInProgress = false;
                rend.material.color = inactiveColor;
            }

            rayEffect.Stop();
        }

        void Update()
        {
            //if (isSelected)
            //{
                //checkPointModel.transform.rotation *= Quaternion.AngleAxis(2f, gameObject.transform.up);
            //}

            // keep the particle system rotation in place
            particleObj.transform.rotation = particleFixedRotation;
        }

        public void SelectCheckPoint()
        {
            isSelected = true;
            rend.material.color = activeColor;
            rayEffect.Play();
        }

        public void DeselectCheckPoint()
        {
            isSelected = false;
            rend.material.color = inactiveColor;
            rayEffect.Stop();
            //rayEffect.Clear();
        }

        public void SetParticleRotation(Quaternion rotation)
        {
            particleFixedRotation = rotation;
        }

        public bool IsVisibleInViewport()
        {
            return rend.isVisible;
        }

        public void ScanningCheckPoint(bool scanning)
        {
            // hide checkpoint when scanning
            ShowCheckPoint(!scanning);

            scanningInProgress = scanning;
        }

        public void CompleteScan()
        {
            isScanned = true;
            isSelected = false;
            scanningInProgress = false;

            gameObject.SetActive(false);
            //rend.material.color = Color.red;
        }

        public bool IsScanned()
        {
            return isScanned;
        }

        public void ShowCheckPoint(bool show)
        {
            if(show)
            {
                rayEffect.Play();
            }
            else
            {
                rayEffect.Stop();
                //rayEffect.Clear();
            }

            checkPointModel.SetActive(show);
        }

        public bool IsScanningNow()
        {
            return scanningInProgress;
        }
    }
}